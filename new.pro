#-------------------------------------------------
#
# Project created by QtCreator 2014-11-25T21:57:11
#
#-------------------------------------------------

QT       += core gui network webkit xml webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = app
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    statkamchatka.cpp

HEADERS  += mainwindow.h \
    statkamchatka.h

FORMS    += mainwindow.ui
