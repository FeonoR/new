#ifndef STATKAMCHATKA_H
#define STATKAMCHATKA_H

#include "QString"
#include "QNetworkRequest"
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QRegExp>

class StatKamchatka : public QObject
{
    Q_OBJECT
public:
    QString log;
    QString pass;
    QString session_key;
    explicit StatKamchatka(QObject *parent = 0);
    ~StatKamchatka();

private:
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
signals:
public slots:
    void Auth();//QString log, QString pass
private slots:
    void AuthAnswers(QNetworkReply*);
    void slotready();
signals:
    void finished();
};

#endif // STATKAMCHATKA_H
