#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "statkamchatka.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;

}

void MainWindow::on_pushButton_clicked()
{
    StatKamchatka *RTK = new StatKamchatka(this);
    RTK -> Auth();
    QString s;
    s = RTK->session_key;

    ui->textEdit->setText(s);
   qDebug() << RTK -> session_key;
}
